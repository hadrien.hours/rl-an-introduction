# Infos

Author: Hadrien Hours
Last update: 2021-01-13

This project aims at storing coding (and non coding) exercises along the following of the book from Richard S. Sutton and Andrew G. Barto `Reinforcement Learning: An introduction` and the Coursera [Reinforcement Learning Specialization](https://www.coursera.org/specializations/reinforcement-learning) from Martha White and Adam White, University of Alberta & Alberta Machine Intelligence Institute

# Content

## Non coding exercises

Along the book there are several exercises in each chapter. There will listed in a document and when time allows solution will be informed.

## Coding exercises

In the book, but more in the online class, there are several coding exercises. Most of them will be implemented in python notebook (Jupyter). In particular, following the on-line course, there will be several assignment in the form of jupyter notebooks. The solutions will be implemented and stored in the folder `Assignments`. 

## Side functions

In parallel, some simple implementations of, for example, the k-armed bandit algorithm, are attempted and present in the main folder. 

* ucb bandit: Upper Confidence Bound Bandit
    - Choose action taking into consideration the uncertainty due to time spent w/o picking a given action
* my first bandit: Epsilon greedy bandit [WIP]

# External resources
* [Richard Sutton slides](http://web.stanford.edu/class/cme241/lecture_slides/rich_sutton_slides/)
* [Book exercises soltuions](http://fumblog.um.ac.ir/gallery/839/weatherwax_sutton_solutions_manual.pdf) is listing solutions to R. Sutton book exercises
* [Gym](https://gym.openai.com/): Toolkit for developing and comparing RL algos
* [Github project from Denny Britz on RL](https://github.com/dennybritz/reinforcement-learning)
