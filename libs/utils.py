import logging

logger = logging.getLogger(__name__)


def scale_inputs(list_values, list_min=None, list_max=None, scale_factor=1):
    """
    This function rescale the inputs so that the are in the range [0, scale_factor].
    If no range provided, return same values
    :param list_values: values to scale
    :param list_min: list of lower bounds
    :param list_max: list of upper bounds
    :param scale_factor: float
    :return: scaled inputs
    """
    if (list_min is None) or (list_max is None):
        return list_values
    if (len(list_min) != len(list_max)) or (len(list_min) != len(list_values)):
        raise ValueError("Dimension mismatch")

    list_scale_values = []
    for i, v in enumerate(list_values):
        denominator = list_max[i] - list_min[i]
        if denominator == 0:
            raise ValueError("Max = min, 0 division error")
        scaled_v = (v - list_min[i]) / float(list_max[i] - list_min[i])
        list_scale_values.append(scale_factor * scaled_v)
    return list_scale_values
