import random
import numpy as np
import logging
import matplotlib.pyplot as plt

logger = logging.getLogger(__name__)


def argmax(q_values):
    """
    Argmax function that pick one randomly in case of ties (numpy picks first one)
    :param q_values:
    :return: index of the maximum value
    """

    max_v = - np.inf
    ties = []

    for i, q in enumerate(q_values):
        if q > max_v:
            ties = [i]
            max_v = q
        if q == max_v:
            ties.append(i)

    return np.random.choice(ties)


class KBanditEnvironment:
    """
    This class implement a very simple version of the K arm bandit environment.
    """

    def __init__(self, k, mu=0, sigma=1):
        self.K = k
        self.mu = mu
        self.sigma = sigma
        # pick randomly K means from the normal distribution for each arm of the bandit problem
        self.k_means = np.random.normal(loc=self.mu, scale=self.sigma, size=self.K)
        self.k_sigmas = np.ones(self.K) * self.sigma
        self.levers = []
        self.rewards = [[] for _ in range(self.K)]

    def pull_arm(self, k, log_reward=False):
        """
        Generate reward when pulling the k-th arm
        :param k: arm index
        :param log_reward: if set to true will store the values of the reward generated through time
        :return: float, reward
        """

        if k >= self.K:
            logger.error("Arm {} does not exist".format(k))

        rew = np.random.normal(self.k_means[k], self.k_sigmas[k])
        if log_reward:
            self.levers.append(k)
            self.rewards[k].append(rew)
        return rew

    def print_rewards(self):
        """
        Print statistics about rewards generated so far
        :return: figure with boxplots of generated reward per arm
        """
        plt.figure()
        plt.subplot(111)
        plt.boxplot(self.rewards)
        plt.xlabel('K arms')
        plt.ylabel('Rewards')
        plt.title('Distributions of generated reward per arm in K bandit')
        plt.show()


class Agent:
    """
    Basic Agent class
    Randomly pick action
    Sample average estimation of action value
    """

    def __init__(self, k):
        self.q_values = np.zeros(k)
        self.arm_count = np.zeros(k)
        self.last_action = np.nan
        self.state = np.nan
        self.K = k

    def pick_action(self):
        """
        Pick a random action
        :return: integer between 0 and K-1
        """
        return np.random.choice(self.K)

    def agent_start(self, state):
        """
        The first method called when the episode starts, called after
        the environment starts

        :state: initial state
        :return first action
        """
        self.state = state
        self.last_action = self.pick_action()
        return self.last_action

    def take_step(self, reward, state):
        """
        Agent take one step from the state it is in  and the reward it receives, it chooses the next action
        :param reward:
        :param state:
        :return: best action
        """
        self.state = state
        self.arm_count[self.last_action] += 1
        step_size = 1. / self.arm_count[self.last_action]
        prev_q_value = self.q_values[self.last_action]
        self.q_values[self.last_action] = prev_q_value + step_size * (reward - prev_q_value)
        self.last_action = self.pick_action()

        return self.last_action

    def print_stats(self):
        plt.figure()
        plt.bar(range(self.K), self.arm_count, alpha=0.6, label='Arm count')
        plt.plot(range(self.K), self.q_values, label='Arm value estimate')
        plt.title('Agent internal values at step {}'.format(np.sum(self.arm_count)))
        plt.legend()
        plt.show()

    def get_stats(self):
        stats = {}
        for arm in range(self.K):
            stats[arm] = (self.arm_count[arm], self.q_values[arm])


class GreedyAgent(Agent):
    """
    RL Agent that always select the action with the best estimated reward
    """

    def pick_action(self):
        """
        Take the action with maximum value. Randomly pick in case of ties
        :return: integer between 0 and K-1
        """
        return argmax(self.q_values)


class EpsilonGreedyAgent(GreedyAgent):
    """
    This agent selects the best action with probability 1 - epsilon and a random one with probability epsilon
    """

    def __init__(self, k, epsilon):
        super().__init__(k)
        self.epsilon = epsilon

    def pick_action(self):
        """
        Pick best action with probability 1-epsilon. Random one otherwise
        :return:
        """
        rn = random.random()
        if rn < self.epsilon:
            return np.random.choice(self.K)
        else:
            return super().pick_action()
