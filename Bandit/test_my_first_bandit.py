from my_first_bandit import KBanditEnvironment, GreedyAgent
import numpy as np

seed = 1
np.random.seed(seed)

K = 5

# Create bandit env
bandit_k = KBanditEnvironment(K)
for i in range(300):
    k = np.random.choice(range(K))
    bandit_k.pull_arm(k, True)

bandit_k.print_rewards()

agent = GreedyAgent(K)
agent.q_values = [0, 0, 0.5, 0, 0]
agent.arm_count = [0, 1, 0, 0, 0]
agent.last_action = 1
action = agent.take_step(reward=1, state=0)

# make sure the q_values were updated correctly
assert agent.q_values == [0, 0.5, 0.5, 0, 0]

# make sure the agent is using the argmax that breaks ties randomly
assert action == 2
