import numpy as np
import matplotlib.pyplot as plt
from libs.tiles3 import tiles, IHT


# Please check documentation for [tiles3](http://incompleteideas.net/tiles/tiles3.html)

def argmax(list_v):
    """
    Re implement argmax to randomly select in case of ties
    :param list_v:
    :return: index best value
    """
    ties = []
    max_v = -np.inf
    for i, v in enumerate(list_v):
        if v > max_v:
            ties = [i]
            max_v = v
        elif v == max_v:
            ties.append(i)
    return np.random.choice(ties)


def get_ucb_value(reward, c, n, t):
    """
    Compute the Upper Confidence Bound for Bandit Algorithm
    :param reward: current reward estimate for this arm
    :param c: degree of exploration
    :param n: number of time this arm was selected
    :param t: number of steps in run
    :return: ucb estimated value
    """
    if n == 0:
        return np.inf
    return reward + c * np.sqrt(np.log(t) / float(n))


class CBAgent:
    """
    Contextual bandit basic entity.
    Assume hashing function or any other technique that translate context in one integer.
    Assume max size of this hashtable pre defined
    Use random action
    Use average reward
    """

    def __init__(self, k, context_size, log=False):
        """
        Object initialization
        :param k: number of arms
        :param context_size: maximum size of context
        :param log: Binary flag to log each event
        """
        self.k = k
        self.d = context_size
        self.last_state = None  # For compatibility. Bandit one single state (state, context)
        self.last_action = None
        self.rewards = np.zeros((k, context_size))
        self.counts = np.zeros((k, context_size))
        self.log = log
        if log:
            self.logs = []

    def get_action(self, state):
        """
        Select next action, random
        :return a random action
        """
        self.last_state = state
        return np.random.choice(range(self.k))

    def agent_start(self, state):
        """
        Start agent
        :param state: initial state (context, arm)
        :return: next action
        """
        self.get_action(state)

    def update_value(self, reward):
        """
        Update value of a given (contextual) arm when receiving a given reward after chosing this arm
        :param reward:
        :return:
        """
        self.counts[self.last_action, self.last_state[0]] += 1
        step_size = 1. / self.counts[self.last_action, self.last_state[0]]
        prev_rew = self.rewards[self.last_action, self.last_state[0]]
        self.rewards[self.last_action, self.last_state[0]] += step_size * (reward - prev_rew)

    def take_step(self, state, reward):
        """
        Return next action after having received next state and reward from previous action.
        This function is for compatibility with other full RL set up.
        :param state: tuple (context, state)
        :param reward:
        :return next_action
        """
        # Update the value of the last taken action with received reward
        self.update_value(reward)
        # Log example if log enabled
        if self.log:
            self.logs.append([self.last_state, self.last_action, reward])
        # Get next action
        self.last_action = self.get_action(state)
        return self.last_action

    def get_stats(self):
        """
        Get agent internal variables
        :return: list of counts and reward per arm
        """
        return [[self.counts[arm, :], self.rewards[arm, :]] for arm in range(self.k)]

    def plot_arm_stats(self):
        # Average cross contexts
        arm_stats = self.get_stats()
        arm_val = [np.mean(_[1]) for _ in arm_stats]
        arm_count = [np.mean(_[0]) for _ in arm_stats]
        x_axis = range(self.k)
        # Create plot
        # Average cross contexts
        fig, ax1 = plt.subplots()
        ax1.plot(x_axis, arm_val, color='b', label='Average arm value')
        ax1.set_xlabel('Arm')
        ax1.set_ylabel('Average value', color='b')
        ax1.tick_params(axis='y', labelcolor='b')
        #
        ax2 = ax1.twinx()
        ax2.set_ylabel('Arm count', color='r')
        plt.bar(x_axis, arm_count, color='r', alpha=0.6, label='Number selections')
        ax2.tick_params(axis='y', labelcolor='r')
        plt.legend()
        plt.title('Arm stats averages across contexts')
        fig.tight_layout()
        return fig


class CBGreedyAgent(CBAgent):
    """
    Contextual Bandit Agent. Using greedy to selection action
    """

    def get_action(self, state):
        """
        Select next action. Greedy
        :return: best current action
        """
        self.last_state = state
        list_reward = self.rewards[:, self.last_state[0]]
        return argmax(list_reward)


class CBEpsilonAgent(CBGreedyAgent):
    """
    Contextual bandit agent, using epsilon greedy when selection action
    """

    def __init__(self, k, epsilon, context_size, log=False):
        """
        Initialize epsilon agent
        :param k: number of action
        :param epsilon: percentage of random exploration
        :param context_size: hash table size
        :param log: flag to log action
        """
        super.__init__(k, context_size, log)
        self.epsilon = epsilon

    def get_action(self, state):
        """
        Select next action. Best action with probability 1-eps. Random with probability eps/k
        :return: action
        """
        if np.random.random() > self.epsilon:
            action = super.get_action(state)
        else:
            self.last_state = state
            action = np.random.choice(self.k)
        return action


class CBUCBAgent(CBAgent):
    """
    Contextual bandit agent, using upper confidence bound to pick action
    """

    def __init__(self, k, c, context_size, log=False):
        """
        Initialize UCB Bandit Agent
        :param k: NUmber of actions
        :param c: importance of uncertainty factor
        :param context_size: size of hash table
        :param log: flag to log action
        """
        super.__init__(k, context_size, log)
        self.c = c

    def get_action(self, state):
        self.last_state = state
        context = self.last_state[0]
        tot_actions = np.sum(self.counts[:, context])
        list_values = []
        for _ in range(self.k):
            list_values.append(get_ucb_value(self.rewards[_, context], self.c, self.counts[_, context], tot_actions))
        return argmax(list_values)


class CBUCBTHAgent(CBUCBAgent):
    """
    Contextual bandit, using upper confidence bound to pick action
    Implement hashing and tiling from tiles3: http://incompleteideas.net/tiles/tiles3.html
    """

    def __init__(self, k, c, hash_size, n_tilings, n_tiles=10, range_contexts=None, log=False):
        """
        Initialize Agent
        :param k: number of arms
        :param c: uncertainty importance factor
        :param hash_size: size of hash table
        :param n_tilings: number of tiling
        :param n_tiles: number of tiles (squared tiling)
        :param range_contexts: array of range of each dimension of context [[min,max],...[min,max]]
        :param log: Flag to log events (default False)
        """
        super.__init__(k, c, n_tilings, log)
        self.iht = IHT(hash_size)
        self.n_tiles = n_tiles
        if range_contexts is not None:
            self.min_vcontext = [_[0] for _ in range_contexts]
        if range_contexts is not None:
            self.max_vcontext = [_[1] for _ in range_contexts]

    def encode_context(self, context):
        """
        Encode the context to fit the tiling
        :param context: array of context values
        :param maxvs : array of maximum value for each dims (optional)
        :param minvs : minimum values for each dims (optional)
        :return:
        """
        if hasattr(self, 'min_vcontext') and hasattr(self, 'max_vcontext'):
            if len(self.max_vcontext) != len(self.min_vcontext) or len(self.max_vcontext) != context:
                raise ValueError("Dimension mismatch")
            context_scaled = []
            for i, v in enumerate(context):
                scaled_v = (v - float(self.min_vcontext[i]))/(float(self.max_vcontext[i] - self.min_vcontext[i]))
                context_scaled.append(self.n_tiles*scaled_v)
        else:
            context_scaled = context

        return tiles(self.IHT, self.d, context_scaled)

    def get_action(self, state):
        encoded_state = self.encode_context(state)
        super().get_action(encoded_state)