import numpy as np
import logging
import random

logger = logging.Logger(__name__)


def get_ucb_value(reward, c, n, t):
    """
    Compute the Upper Confidence Bound for Bandit Algorithm
    :param reward: current reward estimate for this arm
    :param c: degree of exploration
    :param n: number of time this arm was selected
    :param t: number of steps in run
    :return: ucb estimated value
    """
    if n == 0:
        return np.inf
    return reward + c * np.sqrt(np.log(t) / float(n))


def argmax(list_v):
    """
    Re implementation of argmax that randomly breaks ties
    :param list_v:
    :return:
    """
    list_ties = []
    max_v = -np.inf
    for i, v in enumerate(list_v):
        if v == max_v:
            list_ties.append(i)
        elif v > max_v:
            list_ties = [i]
            max_v = v
    return np.random.choice(list_ties)


class BanditArm:
    """
    Multi Arm Bandit Arm object
    """

    def __init__(self, init_value=0):
        self.value = init_value
        self.n = 0

    def update(self, reward):
        """
        Update arm value when having been pulled and receiving a reward
        :param reward:
        :return:
        """
        self.n += 1
        self.value = self.value + (reward - self.value) / float(self.n)


class BanditEnvironment:
    """
    Environment for a multi arm bandit algorithm
    Generate rewards for each pulled arm
    :k int: number of arms
    :mus [float]:mean of each normal distribution for reward of each arm.
    If single value mean of the distribution from which means of arm reward will be drawn
    :sigmas [float]: std of each normal distribution
    If single value std of the distribution from which means of arm reward will be drawn
    """

    def __init__(self, k, mu, sigma):
        self.k = k
        # For logging purposes
        self.rewards = []
        for _ in range(k):
            self.rewards.append([])

        if isinstance(mu, int) and isinstance(sigma, int):
            self.mus = np.random.normal(mu, sigma, k)
            self.sigmas = np.random.normal(mu, sigma, k)
        elif (len(mu) == k) and (len(sigma) == k):
            self.mus = mu
            self.sigmas = sigma
        else:
            raise ValueError("Mu and sigmas must be of length 1 or {k}".format(k=k))

    def generate_reward(self, i):
        """
        Generate reward if ith arm is pulled
        :param i: arm index [0,k[
        :return: reward, float
        """
        if (i < 0) or (i > self.k):
            raise ValueError("This arm does not exist, index should be in [0,{k}[".format(k=self.k))

        reward = np.random.normal(self.mus[i], self.sigmas[i])
        self.rewards[i].append([reward])
        return reward

    def get_statistics(self):
        """
        Return environment parameters and generated rewards so far
        :return:
        """
        return [self.mus, self.sigmas, self.rewards]

    def print_env_values(self):
        mus, sigmas, rews = self.get_statistics()
        print("Arm\tMu\tSigma\tAvgV")
        for i in range(self.k):
            print("{idx}\t{mu}\t{sigma}\t{rew}".format(idx=i, mu=mus[i], sigma=sigmas[i], rew=np.average(rews[i])))


class UcbBanditAgent:
    """
    Upper Confidence Bound Multi Arm Bandit Agent
    : int k: number of arms
    : int c: degree of exploration
    """

    def __init__(self, k, c):
        self.k = k
        self.n = 0
        self.c = c
        self.arms = []
        for i in range(k):
            self.arms.append(BanditArm())

    def pick_arm(self):
        """
        Choose next action, arm, using UCB
        :return int i: index of arm
        """
        return argmax([get_ucb_value(arm.value, self.c, arm.n, self.n) for arm in self.arms])

    def update_arm(self, i, reward):
        """
        After pull i-th, update value accordingly
        :param i: arm index in [0,k[
        :param reward: reward received after pulling i-th arm
        :return:
        """
        self.arms[i].update(reward)

    def run_step(self, env):
        """
        Run one step of the agent
        :env BanditEnvironment: environment
        :return:
        """
        self.n += 1
        arm = self.pick_arm()
        reward = env.generate_reward(arm)
        self.update_arm(arm, reward)

    def get_statistics(self):
        """
        Get arm counts and arm values
        :return:
        """
        arm_counts = [arm.n for arm in self.arms]
        arm_values = [arm.value for arm in self.arms]
        return [arm_counts, arm_values]

    def print_arm_values(self):
        arm_c, arm_v = self.get_statistics()
        print("Arm\tCount\tValue\tUCB")
        for i in range(self.k):
            ucb_val = get_ucb_value(arm_v[i],self.c, arm_c[i], self.n)
            print("{idx}\t{c}\t{v}\t{ucb}".format(idx=i, c=arm_c[i], v=arm_v[i], ucb=ucb_val))
