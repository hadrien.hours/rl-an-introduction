import os, sys
from cb_simpler import CBAgent, CBGreedyAgent, CBEpsilonAgent, CBUCBAgent, CBUCBTHAgent
from BanditEnv import SimpleContextualBanditEnv

sys.path.append('../')

# Test basic CBAgent and environment
k = 5
mu = 3
sigma = 1
context_size = 2
context_ranges = [[-3, 3], [0, 250]]
env = SimpleContextualBanditEnv(k,mu, sigma, context_size, context_ranges, True)
env.print_internals()
print("\n***************")
print("Pull Arm 1")
print("***************")
rew = env.pull_arm(1, [1, 50])
env.print_internals()

