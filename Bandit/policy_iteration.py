#
# This file implements the solution to exercise 4.7 from R. Sutton An introduction to RL
# More specifically, "Write a program for policy iteration and re-solve Jack’s car rental problem" (see Book)
#
import numpy as np

# Default initialization value for states norm rand 0, 1
LOC = 0
SCALE = 1


class State:
    """
    Simple class for a given RL state
    """

    def __init__(self, index, s, a, gamma, final=False):
        """
        Initialization of a state
        :param index: state index
        :param s: total number of states
        :param a: total number of actions
        :param gamma: discounted rate
        :param final: boolean, indicates if final state
        """
        self.gamma = gamma
        self.index = index
        if final:
            self.value = 0
        else:
            self.value = np.random.normal(loc=LOC, scale=SCALE)

    def update(self, states, policy):
        """
        Update the value of a given state given the value of other states and a given policy
        :param states:
        :param policy:
        :return:
        """
        values = states.get_values()
        action = policy.get_action(self.index)
        transition_probabilities = states.get_state_transition_probability(self.index, action)
        transition_rewards = states.get_state_transition_rewards(self.index, action)
        self.value = transition_probabilities * (transition_rewards + values)


class States:
    """
    This class keeps track of all states
    """
    def __init__(self, s, gamma, theta):
        """
        Initialized the set of states
        :param s: Total number of states
        :param gamma: discounted rate for computing return
        :param theta: minimum improvement for policy evaluation
        """
        self.states = np.array(s, dtype=State)
        self.s = s
        self.gamma = gamma
        self.theta = theta

        for s in self.states:
            # TODO: Implement final state initialization
            s.init(s, gamma)

    def get_values(self):
        """
        Get the current values of all states
        :return: numpy array of size |S| with all current state values
        """
        values = np.zeros(self.s)
        for i, s in enumerate(self.states):
            values[i] = s.value

    def update(self):
        """
        Implement policy evaluation
        :return:
        """
        # Initialize the value to twice delta to make sure to enter the loop at first
        delta = self.theta*2
        while delta > self.theta:
            delta = 0
            for s in self.states:
                v_old = s.value
                s.update(self.states)
                v_new = s.value
                delta = max(delta, abs(v_new - v_old))

