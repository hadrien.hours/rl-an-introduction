import numpy as np
import logging,sys
from collections import Counter

# Add path to personal functions
sys.path.append('../')

logger = logging.getLogger(__name__)
# personal modules
from libs.tiles3 import tiles, IHT
from libs.utils import scale_inputs


class SimpleBanditEnv:
    """
    Simple k arm environment. Pick reward from normal distributions
    """

    def __init__(self, k, mu, sigma, log=False):
        self.k = k
        self.mu = mu
        self.sigma = sigma
        self.log = log
        # Generate if needed the parameters of the normal distribution for each arm
        if (isinstance(mu, int) or isinstance(sigma, int)) or (isinstance(mu, float) or isinstance(sigma, float)):
            self.mus = np.random.normal(mu, sigma, k)
            self.sigmas = np.abs(np.random.normal(0, sigma, k))
        elif len(mu) == 2 and len(sigma) == 2:
            self.mus = np.random.normal(mu[0], mu[1], k)
            self.sigmas = np.abs(np.random.normal(sigma[0], sigma[1], k))
        elif len(mu) == k and len(sigma) == k:
            self.mus = mu
            self.sigmas = sigma
        else:
            raise ValueError('Mu ang sigma should be scalar of or vectors of length 2 or k')

        if self.log:
            self.arms = []
            self.rewards = []

    def pull_arm(self, k):
        """
        Generate a random reward after pulling k-th arm
        :param k: arm index
        :return: reward
        """
        reward = np.random.normal(self.mu[k], self.sigmas[k])
        if self.log:
            self.arms.append(k)
            self.rewards.append(reward)

        return reward

    def print_internals(self):
        print("\n*****************")
        print("ENV INTERNALS")
        print("*****************")
        counters = {_: 0 for _ in range(self.k)}
        values = {_: 0 for _ in range(self.k)}
        rewards = {_: None for _ in range(self.k)}

        if self.log:
            for i, v in zip(self.arms, self.rewards):
                counters[i] += 1
                values[i] += v
            for _ in range(self.k):
                if counters[_] != 0:
                    rewards[_] = values[_] / float(counters[_])
        for arm in range(self.k):
            print('{k}\t{mu}\t{sigma}\t{counter}\t{rew}'.format(k=arm, mu=self.mus[arm], sigma=self.sigmas[arm],
                                                                counter=counters[arm], rew=rewards[arm]))

    def get_stats(self):
        """
        Return history of arm pulled and reward and counter of arm pulled and average/std reward per arm
        :return:
        """
        if not self.log:
            logger.info("Information was not logged so nothing to return")
            return
        else:
            arm_counts = Counter(self.arms)
            arm_rewards = {_: [] for _ in range(self.k)}
            for i, v in zip(self.arms, self.rewards):
                arm_rewards[i].append(v)
            arm_stats = [np.average(arm_rewards[_], np.std(arm_rewards[_])) for _ in range(self.k)]
            return self.arms, self.rewards, arm_counts, arm_stats


class SimpleContextualBanditEnv(SimpleBanditEnv):
    """
    Simple Bandit environment, accepting context.

    Each context values are deterministically mapped to a number that deviate the mean of the arm distribution
    Parameters:
        k: number of arms
        mu:
            if scalar: mean of normal distribution from which means of each arm is pulled
            if len 2: [mean, std] of normal distribution from which mean of each arm is pulled
            if len k: list of means of each arm
        sigma:
            Same as mu for std
        context_size: size of vector representing context
        context_ranges: [[min, max],... [min, max]]: list of range for each context dimension
        log: Boolean flag to log reward generated and arm pulled, default is False
    """

    n_tiling = 10

    def __init__(self, k, mu, sigma, context_size, context_ranges, log=False):
        """
        initiate class instance
        """
        if len(context_ranges) != context_size:
            logger.error("Dimension mismatch in contexts")
        super(SimpleContextualBanditEnv, self).__init__(k, mu, sigma, log)
        self.d = context_size
        # Hashing for the context
        table_size = min(2048, 2 ** (2 * context_size))
        self.iht_0 = IHT(2 ** table_size)
        self.minvalues = [_[0] for _ in context_ranges]
        self.maxvalues = [_[1] for _ in context_ranges]
        # To map the hash to a single number
        self.iht_1 = IHT(2 ** self.n_tiling)
        self.range_mus = np.max(self.mus) - np.min(self.mus)
        # To log
        if self.log:
            self.arms = []
            self.rewards = []
            self.contexts = []

    def generate_norm_pars(self, k, context):
        """
        This function generate mu and sigma to be used when pulling an arm within a given context
        :param k: arm index
        :param context: list of values representing context
        :return: mu, sigma
        """
        scaled_values = scale_inputs(context, self.minvalues, self.maxvalues)
        code_0 = tiles(self.iht_0, self.n_tiling, scaled_values)
        code_1 = tiles(self.iht_1, 1, code_0)
        # tiles will first assign 0
        scale_factor = 0.1 + 0.1 * code_1[0] * self.range_mus
        return self.mus[k] * scale_factor, self.sigmas[k]

    def pull_arm(self, k, context):
        """
        Generate reward when pulling arm k within a given context
        :param k: scalar
        :param context: list of float representing context
        :return: reward
        """
        mu, sig = self.generate_norm_pars(k, context)
        reward = np.random.normal(mu, sig)
        if self.log:
            self.arms.append(k)
            self.rewards.append(reward)
            self.contexts.append(context)

        return reward
