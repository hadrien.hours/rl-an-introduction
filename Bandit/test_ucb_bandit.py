from ucb_bandit import BanditEnvironment
from ucb_bandit import UcbBanditAgent
from ucb_bandit import get_ucb_value
from tqdm import tqdm
# Case 1
# Arms
k = 4
# Environment variables
mu = 2
sigma = 1
#  Agent variables
c = 0.1
# Simulation variables
N = 100

env = BanditEnvironment(k, mu, sigma)
agent = UcbBanditAgent(k, c)

env.print_env_values()
agent.print_arm_values()

for step in tqdm(range(N)):
    agent.run_step(env)
    if step % 10 == 0:
        print("\n***********\n\t STEP {}\n*************\n".format(step))
        env.print_env_values()
        agent.print_arm_values()
        print("**************")
